using NUnit.Framework;
using ZMachineDotNet.Types;
using ZMachineDotNet.Types.Opcode;

namespace ZMachineDotNet.Test;

public class ZInstructionShould
{
    [Test]
    [TestCase((byte) 0b10110000, Kind.ZeroOp)] // Short instruction (0OP:$0)
    [TestCase((byte) 0b10110101, Kind.ZeroOp)] // 0OP:$5
    [TestCase((byte) 0b10000000, Kind.OneOp)]  // Short instruction (1OP:$0)
    [TestCase((byte) 0b10010110, Kind.OneOp)]  // 1OP:$6
    [TestCase((byte) 0b01000000, Kind.TwoOp)]  // Long instruction (2OP:$0)
    [TestCase((byte) 0b01101100, Kind.TwoOp)]  // 2OP:$C
    [TestCase((byte) 0b11000000, Kind.TwoOp)]  // Variable instruction (2OP)
    [TestCase((byte) 0b11100000, Kind.Var)]    // Variable instruction (VAR)
    [TestCase((byte) 0b11101100, Kind.Var)]    // VAR:$C
    [TestCase((byte) 0b11111010, Kind.Var)]    // VAR:$1A
    public void ZInstructionShouldReturnCorrectKind(byte testByte, Kind expectedKind)
    {
        Assert.That(ZInstruction.GetOpCodeKind(testByte), Is.EqualTo(expectedKind));
        
    }
}