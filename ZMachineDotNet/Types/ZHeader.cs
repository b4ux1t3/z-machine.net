﻿namespace ZMachineDotNet.Types;

public record ZHeader
{
    public ZWord FirstInstructionAddress { get; }
    public ZWord DictionaryAddress { get; }
    public ZWord ObjectTableAddress { get; }
    public ZWord GlobalVariablesAddress { get; }
    public ZWord StaticMemoryAddress { get; }
    private byte[] _bytes;
    private const int FirstInstructionAddressOffset = 0x06;
    private const int DictionaryAddressOffset = 0x08;
    private const int ObjectTableAddressOffset = 0x0a;
    private const int GlobalVariableAddressOffset = 0x0c;
    private const int StaticMemoryAddressOffset = 0x0e;
    

    public ZHeader(byte[] bytes)
    {
        if (bytes.Length != 64) throw new InvalidHeaderLengthException(bytes.Length);
        _bytes = bytes;
        // Looks like the Z-machine is big endian.
        FirstInstructionAddress = (ZWord) bytes[ZHeader.FirstInstructionAddressOffset] << 8 | ((ZWord) bytes[ZHeader.FirstInstructionAddressOffset + 1]);
        DictionaryAddress = (ZWord) bytes[ZHeader.DictionaryAddressOffset]  << 8 | ((ZWord) bytes[ZHeader.DictionaryAddressOffset + 1]);
        ObjectTableAddress = (ZWord) bytes[ZHeader.ObjectTableAddressOffset] << 8 | (ZWord) bytes[ZHeader.ObjectTableAddressOffset + 1];
        GlobalVariablesAddress = (ZWord) bytes[ZHeader.GlobalVariableAddressOffset] << 8 | (ZWord) bytes[ZHeader.GlobalVariableAddressOffset + 1];
        StaticMemoryAddress = (ZWord) bytes[ZHeader.StaticMemoryAddressOffset] << 8| (ZWord) bytes[ZHeader.StaticMemoryAddressOffset + 1];
    }
    
    
}