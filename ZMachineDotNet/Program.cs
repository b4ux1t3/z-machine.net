﻿using System;
using System.IO;
using McMaster.Extensions.CommandLineUtils;

namespace ZMachineDotNet;

// ReSharper disable once ClassNeverInstantiated.Global
public class Program
{
    public static void Main(string[] args) => CommandLineApplication.Execute<Program>(args);

    [Option(Description = "Rom File", LongName = "rom-file", ShortName = "rf")]
    private string RomFilePath { get; } = "data/zdungeon.z5";

    private void OnExecute()
    {
        var loadedZProgram = Array.Empty<byte>();
        if (File.Exists(RomFilePath))
        {
            using var inputFileStream = File.OpenRead(RomFilePath);
            var fileLength = (int) inputFileStream.Length;
            loadedZProgram = new byte[fileLength];
            inputFileStream.Read(loadedZProgram, 0, fileLength);
        }

        if (loadedZProgram is {Length: < 1})
        {
            Console.WriteLine($"Could not load Z-Program from {RomFilePath}");
            Environment.Exit(1);
            return;
        }

        var myMachine = new ZMachine(loadedZProgram);
        try
        {
            myMachine.Initialize();
        }
        catch (NotImplementedException e)
        {
            Console.WriteLine(e.Message);
        }
    }
}