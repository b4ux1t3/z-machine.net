﻿# References

Try to keep references in the following format, obviously omitting any irrelevant data:
```
[<Name>](<Link>) - <Author> (<Publish date>) <Page Number(s)>
```
## Specifications
[The Z-machine, And How To Emulate It](http://mirrors.ibiblio.org/interactive-fiction/infocom/z-machine/zspec02/zmach06e.pdf) - Marnix Klooster (1996)