﻿# ToDos!
## Unit tests
* Each of the parsing methods need to be tested: 
  * Ideally, we need to test _every defined opcode_. This actually won't be that hard, since there aren't that many opcodes. You can find them in table 7 in the [spec](http://mirrors.ibiblio.org/interactive-fiction/infocom/z-machine/zspec02/zmach06e.pdf).
  * Need to make sure `GetOpCodeKind` always correctly parses the `Kind` of opcode.
  * Need to make sure `GetVarOperands` always gets the appropriate number of arguments when we parse `VAR` opcodes.
